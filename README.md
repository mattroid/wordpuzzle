Valant Excercise - Puzzle API
======================

How it works
------------
This solution uses a Trie prefix tree to store all the valid words and match when traversing the board.

The Trie is setup with one node for every letter (26 nodes). 
Additionally there is a `Count` field that tracks the number of words contained in each sub tree. This way we can finish traversing tiles early if the prefix isn't going to match anything moving forward. 


Performance
-----------
Below are some benchmarks

Run on 2015 Macbook Pro with i7 CPU

```
Board Size: 10 X 10
Trie loaded in 180ms
Time to search: 163ms
Number of Matches 108
```
```
Board Size: 50 X 50
Trie loaded in 202ms
Time to search: 188ms
Number of Matches 2911
```
```
Board Size: 102 X 102
Trie loaded in 186ms
Time to search: 244ms
Number of Matches 6418
```
```
Board Size: 502 X 502
Trie loaded in 175ms
Time to search: 18036ms
Number of Matches 24617
```
```
Board Size: 602 X 602
Trie loaded in 195ms
Time to search: 35973ms
Number of Matches 27346
```

How to start
------------
Project was built with dotnet core 2.1 Though could probably be run on most versions of .Net. Microsoft has spent some good time improving their dotnet core performance so I suspect the best performance will be with this version. 

```
dotnet restore
dotnet run --project WordPuzzle
```

Testing
-------
```
Unit Testing for Puzzle Library
```

Manually test
-------------
The below will ask the api for all words found in a 10x10 board of random letters.

There's also a generate_string.py script to generate new boards of different sizes

```
curl -X POST \
  http://localhost:5000/api/board \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '"gnspofbxrfqfwttiokttxvnoigvjahuwystrwenpvfzrpgbrblfolqgcfkruwsfdwmozrjxloqqnyzrktlojuukyyxmrsuirmyvf"'
```

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using WordPuzzle.Puzzle;

namespace WordPuzzle.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BoardController : ControllerBase
    {
        private readonly IPuzzleService puzzleService;
        
        public BoardController(IPuzzleService puzzleService)
        {
            this.puzzleService = puzzleService;
        }
      
        /*
         * Returns a list of words that were matched. This is a subset of the words submitted through the URL
         *
         * If no words were matched will return empty string
         */
        // POST api/board/wordToFind
        [HttpPost("{word}")]
        public ActionResult<string> Post([FromBody] string board, string word)
        {
            var foundWords = puzzleService.SearchBoard(board, new[] {word});
            
            return foundWords.Join(",");
        }
        
        /*
         * Returns a list of words that were matched
         *
         * If no words were matched will return empty string
         */
        // POST api/board
        [HttpPost]
        public ActionResult<string> Post([FromBody] string board)
        {
            var foundWords = puzzleService.SearchBoard(board);
            
            return foundWords.Join(",");
        }
    }
}
using System;

namespace WordPuzzle.Puzzle
{
    /*
     * Sets up a board given a string of characters. Since the board is always NxN size we can just square the string
     * length to calculate board dimensions
     * So "abcdefghi" would get scored to a 3x3 board and the navigation would treat it like it's
     *  ---------
     *  | a b c |
     *  | d e f |
     *  | g h i |
     *  --------- 
     *
     *  Additionally the board sets up a border so that the traversal can detect the edges and not go out of bounds. The
     *  above board end result would look like the following
     *  -------------
     *  | 0 0 0 0 0 |
     *  | 0 a b c 0 |
     *  | 0 d e f 0 |
     *  | 0 g h i 0 |
     *  | 0 0 0 0 0 |
     *  -------------
     */
    public class Board
    {
        public int[] Tiles { get; }
        public int Size { get; }
        public int[] Directions { get; }
        
        public Board(string boardChars)
        {
            // calculate board size
            var columns = Convert.ToInt32(Math.Sqrt(boardChars.Length) + 2);
            Size = columns * columns;
            Tiles = new int[Size];

            // Offset values for each direction our board can traverse
            Directions = new[] {-1-columns, -columns, 1-columns, -1, 1, columns-1, columns, columns+1};
            
            var j = 0;
            for (var i = 0; i < Size; i++)
            {
                if (i < columns || // top
                    (i + 1) % columns == 0 || // right 
                    i > columns * (columns - 1) || // bot
                    i % columns == 0) // left
                {
                    Tiles[i] = Trie.NUM_CHARACTERS; // Assign boarder of board to index 26
                } else
                {
                    Tiles[i] = char.ToUpper(boardChars[j]) - 64;
                    j++;
                }
            }
        }
    }
}
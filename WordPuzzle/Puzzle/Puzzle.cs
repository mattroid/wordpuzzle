using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;

namespace WordPuzzle.Puzzle
{
    /*
     * Puzzle class searches boards and finds all solutions
     *
     * Pass in a board as a string "abcdefghi" and load valid words it will produce all words given the following rules:
     *  1. Words are tiles in a sequence moving in 8 directions up, down, left, right and diagonals.
     *  2. Words are at least 3 characters long
     *  3. Tiles can not be used more than once
     */
    public class Puzzle
    {
        // Number of directions a board can be traversed
        private const int NUM_DIRECTIONS = 8; 
        public Trie TrieStore { get; private set; }
        public HashSet<string> Words { get; private set; }
        
        public IEnumerable<string> SearchBoard(string boardChars)
        {
            // check for boards that are too large to process 
            if (boardChars.Length > 360000) throw new Exception("Board is too large. Processing this board will exceed maximum processing time of 30 seconds");
                
            var board = new Board(boardChars);
            var stopwatch = Stopwatch.StartNew();
            
            LoadTrie(Words);
            
            stopwatch.Stop();
            Console.WriteLine("Trie loaded in {0}ms", stopwatch.ElapsedMilliseconds);
            
            return WalkBoard(board);
        }

        public IEnumerable<string> SearchBoard(string boardChars, string[] wordsToFind)
        {
            // check for boards that are too large to process 
            if (boardChars.Length > 360000) throw new Exception("Board is too large. Processing this board will exceed maximum processing time of 30 seconds");
            
            var board = new Board(boardChars);
            LoadTrie(wordsToFind);
            return WalkBoard(board);
        }
        
        //for each cube on the board, perform a depth first search using descend()
        private string[] WalkBoard(Board board)
        {
            if (TrieStore == null) return new string[]{};
            
            var subTree = TrieStore;
            var searched = new bool[board.Size];
            var j = 0;

            var stopwatch = Stopwatch.StartNew();
            
            // traverse a path for each tile on the board
            for (var i = 0; i < board.Size; i++)
            {
                // Reset visited tiles
                Array.Clear(searched, 0, board.Size);
                
                if (board.Tiles[i] == Trie.NUM_CHARACTERS) continue;
                
                // Depth First Search
                WalkBoardDescend(board, i, subTree, searched);
                
                j++;
            }

            // walk the trie for words that were identified
            var foundWords = new List<string>();
            GetFoundWords(subTree, foundWords);
            
            stopwatch.Stop();
            Console.WriteLine("==================================================");
            Console.WriteLine("Board Size: {0} X {0}", Math.Sqrt(board.Size));
            Console.WriteLine("Time to search: {0}ms", stopwatch.ElapsedMilliseconds);
            Console.WriteLine("Number of Matches {0}", foundWords.Count);
            Console.WriteLine("==================================================");
            
            return foundWords.ToArray();
        }
        
        private void WalkBoardDescend(Board board, int tileIndex, Trie subTree, bool[] searched)
        {
            subTree = LookupWord( board.Tiles[tileIndex], subTree);

            // only descend if there are still words left in this prefix subtree
            if (subTree?.Count == null || subTree.Count <= 0) return;

            var searchedFromHere = new bool[board.Size];
            
            searched.CopyTo(searchedFromHere,0);
            //mark this tile as already searched
            searchedFromHere[tileIndex] = true; 

            // branch our search for each possible direction 
            for (var i = 0; i < NUM_DIRECTIONS; i++){
                var nextTileIndex = tileIndex + board.Directions[i];
                if(board.Tiles[nextTileIndex] != Trie.NUM_CHARACTERS && !searchedFromHere[nextTileIndex])
                    WalkBoardDescend(board, nextTileIndex, subTree, searchedFromHere);
            }
        }
        
        private void GetFoundWords(Trie subTree, List<string> found)
        {
            if (subTree == null) return;
            
            if (subTree.Found)
                found.Add(subTree.Word);

            for (var i = 0; i < Trie.NUM_CHARACTERS; i++){
                GetFoundWords(subTree.Children[i], found);
            }
        }
        
        //returns a Trie* to the next prefix or NULL
        //keeps track of which words have been found
        private static Trie LookupWord(int i, Trie subTree)
        {
            subTree = subTree.Children[i];
            
            if (subTree?.Word == null || subTree.Found) return subTree;

            subTree.Found = true;
            
            // Decrement how many words are left that use this prefix
            subTree.Parent.Count--;

            return subTree;
        } 
        
        public void LoadWords(string wordsJson)
        {
            var words = JsonConvert.DeserializeObject<Dictionary<string, string>>(wordsJson);
            
            // Just need the keys for the words the values are mapped to the word definitions
            Words = words.Keys.ToHashSet();
        }

        public void LoadTrie(IEnumerable<string> words)
        {
            TrieStore = new Trie();
            // Tree pointer
            Trie p;
            int i;
            foreach (var word in words)
            {
                // start at root trie node
                p = TrieStore;
                
                for (i = 0; i < word.Length; i++)
                {
                    // Skip words less than 3 chars
                    if (word.Length < 3) continue; 
                    
                    var charIndex = char.ToUpper(word[i]) - 64; //convert characters to integers: a=0 z=25

                    // Redirect invalid chars to index 0
                    // This causes all invalid chars to be matched
                    // the same.
                    if (charIndex > 25 || charIndex < 1)
                    {
                        charIndex = 0;
                    }
                    
                    p.Count++;
                    
                    if(p.Children[charIndex] == null)
                    {
                        p.Children[charIndex] = new Trie();
                    }

                    p.Children[charIndex].Parent = p;

                    p = p.Children[charIndex];
                }

                p.Word = word;
            }
            
        }

    }

}
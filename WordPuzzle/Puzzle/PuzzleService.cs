using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace WordPuzzle.Puzzle
{
    /*
     * Service for providing state for the API. 
     */
    public interface IPuzzleService
    {
        IEnumerable<string> SearchBoard(string boardChars);
        IEnumerable<string> SearchBoard(string boardChars, string[] wordsToFind);
    }
    public class PuzzleService : IPuzzleService 
    {
        private Puzzle PuzzleInstance { get; }

        public PuzzleService(IHostingEnvironment hostingEnvironment)
        {
            PuzzleInstance = new Puzzle();

            var jsonDictionary = File.ReadAllText(hostingEnvironment.ContentRootPath + @"/dictionary.json");
            
            PuzzleInstance.LoadWords(jsonDictionary);
        }

        public IEnumerable<string> SearchBoard(string boardChars)
        {
            return PuzzleInstance.SearchBoard(boardChars);
        }

        public IEnumerable<string> SearchBoard(string boardChars, string[] wordsToFind)
        {
            return PuzzleInstance.SearchBoard(boardChars, wordsToFind);
        }
    }
}
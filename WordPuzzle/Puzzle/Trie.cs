namespace WordPuzzle.Puzzle
{
    public class Trie
    {
        /*
         *  This is a Trie Data Structure a form on nested tree's used for prefix matching on strings
         *  https://en.wikipedia.org/wiki/Trie
         */
        public const int NUM_CHARACTERS = 26;
        
        public Trie()
        {
            Children = new Trie[NUM_CHARACTERS];
            Found = false;
        }
        public Trie[] Children { get; }
        public Trie Parent { get; set; }
        
        // Indicate the number of words at or below this prefix
        public int Count { get; set; }
        public string Word { get; set; }
        public bool Found { get; set; }
        
    }
}
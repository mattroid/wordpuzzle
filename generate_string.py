import random
import string
import sys

print(''.join([random.choice(string.ascii_lowercase) for n in xrange(int(sys.argv[1])**2)]))
